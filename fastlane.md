# Fastlane

Fast lane installation and use documentation.

---

## Overview

Fastlane is the tool to release your iOS and Android application It handles all tedious tasks, like generating screenshots, dealing with code signing, and releasing your application.

### Why should i use Fastlane?

* Save hours every time you push a new release to the store or beta testing service.
* Integrates with all your existing tools and services (170 actions currently).
* 100% open source under the MIT license.
* Easy setup assistant to get started in a few minutes.
* Runs on your machine, it's your app and your data.
* Integrates with all major CI systems.
* Supports iOS, Mac, and Android apps.
* Extend and customise fastlane to fit your needs, you're not dependent on anyone.
* Never remember any commands any more, just fastlane.
* Deploy from any computer, including a CI server


---

## Installation

### System Requirement:-

* Make sure you have Xcode installed.
* Fastlane requires macOS or Linux with Ruby 2.0.0 or above

### Using the terminal

To install fastlane.

```bash
sudo gem install fastlane --verbose
```

Go to your iOS app and run it to initialise fastlane.

```bash
fastlane init
```
![Add Python to PATH](img/fastlaneInit.png)

Fastlane will automatically detect your project, and ask for any missing information.

---


## Beta Deployment
To upload to appstore.

### Building your app

Fastlane takes care of building your app using a feature called gym, just add the following to your “Fastfile” file in your project:

```bash
lane :beta do
# match(type: "appstore") # more information: https://codesigning.guide
#gym # Build your app - more options available
cert
sigh
gym(scheme: “Your Projet Name“,
workspace: “YourProject.xcworkspace",
include_bitcode: true)
```
Try running the lane using:-

```bash
fastlane beta
```

If everything works fine, you should have a [ProductName].ipa file in the current directory.

## Uploading App

After building your app, it's ready to be uploaded to a beta testing service of your choice. The beauty of fastlane is that you can easily switch beta provider, or even upload to multiple at once, without any extra work.

All you have to do is to put the name of the beta testing provider of your choice after the app using gym:

```bash
lane :beta do
# match(type: "appstore") # more information: https://codesigning.guide
#gym # Build your app - more options available
cert
sigh
gym(scheme: “Your Projet Name“,
workspace: “YourProject.xcworkspace",
include_bitcode: false)
testflight
end
```

Fastlane automatically passes on information about the generated .ipa file from gym to the beta testing provider of your choice.

#### Screenshot:-

The following is the screenshot of Well Engineering app that uses fastlane to upload to App Store.
![Add Python to PATH](img/uploading_app.png)

You can also send message on Slack when the app is uploaded by adding th following lines.

```bash
lane :beta do
# match(type: "appstore") # more information: https://codesigning.guide
#gym # Build your app - more options available
cert
sigh
gym(scheme: “Your Projet Name“,
workspace: “YourProject.xcworkspace",
include_bitcode: false)
testflight
slack(message: "Successfully distributed a new beta build")
end
```

## Fastlane supports these beta testing services

* TestFlight
* Crashlytics
* HockeyApp
* TestFairy 



## Fastlane Commands

### Some commonly used Fastlane commands:

* `sudo gem install fastlane --verbose` - To install fastlane.
* `fastlane init` - Go to your iOS app and run it to initialise fastlane.
* `fastlane beta` - To upload to appstore.


There are various other commands and options available. For a complete list of
commands, run `fastlane help` :

```bash
fastlane help
```


## Getting help

For full documentation visit [https://docs.fastlane.tools/](https://docs.fastlane.tools/).
